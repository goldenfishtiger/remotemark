Remote Bookmark
========

リモートに配置し、どの環境からも使える自分用のブックマークサイト


使い方
--------

#### ブックマークしたURLへジャンプする

ジャンプしたいブックマークをクリックする。
ドロップダウンボックスの中に入っている場合も同様。

※クリックした場合、対象のブックマークは、内部的にリストの先頭に移動する。
reloadを押すと、最新の状態に更新される。


#### ブックマークの新規登録

uriのテキストボックスに、ブックマークしたいURLをペーストし「set」ボタンを押す。


#### ブックマークの名前変更

ブックマークの名前は、対象ページのtitleタグから自動的に取得されるが、
取得できなかったり、取得された名前が適切でなかったりする場合がある。
その場合は、対象のブックマークのチェックボックスをチェックした上で、
nameのテキストボックスに、好みの名前を入力し「set」ボタンを押す。


#### ブックマークの削除

対象のブックマークのチェックボックスをチェックし「remove」ボタンを押す。


#### ユーザの登録

reloadを押し、アドレスバーのURI末尾の「…?user=nobody」のnobodyを、
好みのユーザ名(yamada等)に修正し、そのURIに移動する。

※ブックマークの表示順や、フォルダの分類は、ユーザごとに管理される。
それまで登録されていた(他のユーザの登録した)ブックマークは、すべてUnVisitedに移動する形となる。
UnVisitedの中のブックマークにジャンプすると、内部的にVisitedの先頭に移動する。
reloadを押すと、最新の状態に更新される。

※ユーザを削除するUIはない


#### ブックマークフォルダの作成

フォルダにまとめたい、複数のブックマークのチェックボックスをチェックした上で、
nameのテキストボックスに、フォルダの名前を入力し「set」ボタンを押す。


#### 既存のブックマークフォルダへの追加

フォルダに追加したい、ブックマークのチェックボックスをチェックした上で、
追加先のフォルダのチェックボックスをチェックし「set」ボタンを押す。  

※特定のブックマークをフォルダから削除するUIはない。


#### ブックマークフォルダの最小化

フォルダ名の右の「x」を押す。
ドロップダウンボックスの中に入った形となる。


#### ブックマークフォルダの部分開示

フォルダ名の右の「+」を押す。
繰り返し「+」を押すと、開示数が増える。

※開示数を減らすUIはない。


#### ブックマークフォルダの全開示

フォルダ名の右の「o」を押す。


#### フォルダの名前変更

対象のフォルダのチェックボックスをチェックした上で、
nameのテキストボックスに、好みの名前を入力し「set」ボタンを押す。


#### ブックマークフォルダの削除

削除したいフォルダのチェックボックスをチェックし「remove」ボタンを押す。  
フォルダの中にあったブックマークは、Visitedに移動する。

